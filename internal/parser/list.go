package parser

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

func (g Grabber) ListOffers() func(*cobra.Command, []string) {
	return func(cmd *cobra.Command, args []string) {
		// show item
		offers := g.Parser.ReadMDFile(baseDir + "products/items.md")

		for i, offer := range offers {
			fmt.Println(
				fmt.Sprintf(
					"%s %s",
					ColorString(strconv.Itoa(i+1), "green", "bold"),
					offer.StringShort(),
				),
			)
		}

	}
}
