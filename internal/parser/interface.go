package parser

import (
	"github.com/spf13/cobra"
)

func MainInterface() func(*cobra.Command, []string) {
	return func(cmd *cobra.Command, args []string) {
		parser := Parser{}.Init()
		grabber := Grabber{
			Parser: parser,
		}

		grabber.GrabProfile()
	}
}
