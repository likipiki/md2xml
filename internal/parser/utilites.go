package parser

import (
	"fmt"
	"os/exec"
	"runtime"
)

// Colors
var (
	colors = map[string]int{
		"white":  0,
		"red":    1,
		"green":  2,
		"orange": 3,
		"blue":   4,
		"pink":   5,
		"cyan":   6,
		"gray":   7,
	}

	colorTypes = map[string]int{
		"bold":       1,
		"normal":     5,
		"underline":  4,
		"background": 7,
	}
)

func ColorString(text, color, font string) string {
	if font == "" {
		font = "normal"
	}

	if color == "" {
		color = ""
	}

	return fmt.Sprintf(
		"\033[3%d;%dm%s\033[0m",
		colors[color],
		colorTypes[font],
		text,
	)
}

// OpenImagesFolder, opens image folder in /site/static/<folder>
func OpenImagesFolder(folder string) error {
	command := ""

	switch runtime.GOOS {
	case "linux":
		command = "nautilus"
	case "darwin":
		command = "open"
	}

	cmd := exec.Command(
		command,
		baseDir+"site/static/"+folder,
	)

	cmd.Run()
	return nil
}
