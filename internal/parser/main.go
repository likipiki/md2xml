package parser

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

var (
	withoutCache = false
	rootCmd      = &cobra.Command{
		Use: "md2xml",
	}
)

func init() {
	commands := getCommands()
	for _, command := range commands {
		rootCmd.AddCommand(command)
	}
}

func getCommands() []*cobra.Command {
	grabCommand := &cobra.Command{
		Use:   "grab",
		Short: "Grab avito profile",
		Long:  `Grab avito profile, and save results to ./cache/offer.bin file`,
		Run:   Grabber{}.GrabProfile(),
		Args: func(cmd *cobra.Command, args []string) error {

			for _, arg := range args {
				if _, err := strconv.Atoi(arg); err != nil {
					if arg != "-nc" {
						return fmt.Errorf("Invalid offer number %s", args[0])
					}
				}
			}
			return nil
		},
	}
	grabCommand.PersistentFlags().BoolVarP(&withoutCache, "nocache", "n", false, "Grab without cache")

	return []*cobra.Command{
		&cobra.Command{
			Use:   "parse [no options!]",
			Short: "Create new xml yandex.O file",
			Long:  `Parse items.md file and create xml files with products`,
			Run:   ParseMd("items.md"),
		},
		&cobra.Command{
			Use:   "new [no options!]",
			Short: "Create new item",
			Long:  `Create new item`,
			Run:   NewItem(),
		},
		&cobra.Command{
			Use:   "delete",
			Short: "Delete item",
			Long:  "Delete item from .md file, remove offer folder from disk",
			Run:   Grabber{}.DeleteOffer(),
			Args: func(cmd *cobra.Command, args []string) error {
				if len(args) < 1 {
					fmt.Println(
						ColorString("Nothing to delete, please enter a number...", "red", ""),
					)
					return nil
				}

				if _, err := strconv.Atoi(args[0]); err != nil {
					return fmt.Errorf("Invalid offer number %s", args[0])
				}

				return nil
			},
		},
		&cobra.Command{
			Use:   "sync",
			Short: "Sync avito offers with O.yandex",
			Long:  "Fetch all O.yandex offers and sync it with avito offers",
			Run:   Grabber{}.SyncOffers(),
		},
		&cobra.Command{
			Use:   "list",
			Short: "List of all active items",
			Long:  "Show full list with active items stored in products.md",
			Run:   Grabber{}.ListOffers(),
		},
		grabCommand,
	}
}

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}
