package parser

import (
	"bufio"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gocolly/colly"
)

const (
	avitoUserID = "1346034488ae90a5a9015100f265d97f"
)

type GrabberItem struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	URL         string `json:"url"`
	Price       int    `json:"price"`
	Category    string `json:"-"`
	Description string `json:"-"`
	Directory   string `json:"-"`
}

func (g GrabberItem) String() string {
	return fmt.Sprintf(
		"Title: %s\nPrice: %dP\nDirectory: %s\nCategory: %s\nDescription: %s\nLink: %s\n",
		g.Title,
		g.Price,
		g.Directory,
		g.Category,
		g.Description,
		g.URL,
	)
}

type APIRequest struct {
	Status string `json:"status"`
	Result struct {
		List []GrabberItem `json:"list"`
	} `json:"result"`
}

func GetAllUserOffers(withoutCache bool) []GrabberItem {
	request := APIRequest{}

	if withoutCache {
		avitoApiURL := "https://www.avito.ru/web/1/profile/public/items?hashUserId=" + avitoUserID + "&shortcut=active&offset=0&limit=100"

		resp, err := http.Get(avitoApiURL)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		jsonBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		err = json.Unmarshal(jsonBytes, &request)
		if err != nil {
			panic(err)
		}

		if (len(jsonBytes) > 0) && (!withoutCache) {
			f, err := os.Create("./cache/offers.bin")
			if err != nil {
				log.Fatal("Couldn't open file offers.bin")
			}
			defer f.Close()

			err = binary.Write(f, binary.LittleEndian, jsonBytes)

			fmt.Println(
				ColorString("Cache updated!", "orange", "bold"),
			)
		}

		return request.Result.List
	}

	// Reading from cache file
	f, err := os.Open("./cache/offers.bin")
	defer f.Close()

	if err != nil {
		log.Fatal("Couldn't open file offers.bin")
	}

	stats, err := f.Stat()
	if err != nil {
		panic(err)
	}

	var size int64 = stats.Size()
	bytes := make([]byte, size)

	bufr := bufio.NewReader(f)
	_, err = bufr.Read(bytes)

	err = json.Unmarshal(bytes, &request)
	if err != nil {
		panic(err)
	}

	return request.Result.List
}

// GrabItem fetch Item price and item description from avito
func (g Grabber) GrabItem(link string, item *GrabberItem) {
	c := colly.NewCollector()

	c.OnHTML(".item-description-text", func(e *colly.HTMLElement) {
		html, err := e.DOM.Html()
		if err != nil {
			log.Println(err)
		}
		html = strings.TrimSpace(html)
		html = strings.ReplaceAll(html, "<p>", "")
		html = strings.ReplaceAll(html, "</p>", "\n\n")
		html = strings.ReplaceAll(html, "<br/>", "\n")
		html = strings.TrimRight(html, "\n") // remove last \n\n sequence

		item.Description = html
	})

	c.OnHTML("span.js-item-price", func(e *colly.HTMLElement) {
		priceString := strings.ReplaceAll(e.Text, " ", "")
		price, err := strconv.Atoi(priceString)
		if err != nil {
			panic(err)
		}
		item.Price = price
	})

	c.Visit(avitoURL + link)
}

func (g GrabberItem) Resave(items []GrabberItem) {
	f, err := os.Create(baseDir + "products/items.md")
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	for _, item := range items {
		price := strconv.Itoa(item.Price)
		g.Description = strings.TrimSpace(item.Description)

		if item.Directory == "" {
			item.Directory = "\n"
		}

		str := "# " + item.Title + "\n" + price + "\n" + item.Directory + "\n" + item.Category + "\n" + item.Description + "\n"
		if _, err := f.WriteString(str); err != nil {
			log.Println(err)
		}
	}
}

func (g GrabberItem) SaveToFile() {
	f, err := os.OpenFile(baseDir+"products/items.md", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	price := strconv.Itoa(g.Price)
	g.Description = strings.TrimSpace(g.Description)

	if g.Directory == "" {
		g.Directory = "\n"
	}

	str := "# " + g.Title + "\n" + price + "\n" + g.Directory + "\n" + g.Category + "\n" + g.Description + "\n"
	if _, err := f.WriteString(str); err != nil {
		log.Println(err)
	}
}
