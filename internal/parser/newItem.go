package parser

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

const (
	categorySeparator = "\t"
)

func withFilter(command string) []string {
	shell := os.Getenv("SHELL")
	if len(shell) == 0 {
		shell = "sh"
	}
	cmd := exec.Command(shell, "-c", command)
	cmd.Stderr = os.Stderr

	result, _ := cmd.Output()
	return strings.Split(string(result), "\n")
}

func CategorySelector() string {
	filtered := withFilter("cat " + baseDir + "products/categories.txt | fzf")

	if len(filtered) > 0 {
		result := filtered[0]

		separatorPosition := strings.Index(result, categorySeparator)
		if separatorPosition < 0 {
			return ""
		}

		// return substring with separator \t + 1 index
		return result[separatorPosition+1:]
	}

	return ""
}

func NewItem() func(*cobra.Command, []string) {
	return func(cmd *cobra.Command, args []string) {
		reader := bufio.NewReader(os.Stdin)
		var err error
		item := GrabberItem{}

		fmt.Print("Enter title --> ")
		item.Title, err = reader.ReadString('\n')
		if err != nil {
			log.Println(err)
		}

		fmt.Print("Enter price --> ")
		var priceStr string
		priceStr, err = reader.ReadString('\n')
		if err != nil {
			log.Println(err)
		}
		price, err := strconv.Atoi(priceStr)
		item.Price = price

		var directory string
		directory, err = reader.ReadString('\n')
		if err != nil {
			log.Println(err)
		}
		item.Directory = directory

		for item.Category == "" {
			category := CategorySelector()
			item.Category = category
		}

		OpenImagesFolder(item.Directory)

		// TODO: read description from console <05-02-21, likipiki>

		fmt.Println(item)
	}
}
