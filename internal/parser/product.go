package parser

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"
)

const (
	maxIDLen = 20
	// A generic XML header suitable for use with the output of Marshal.
	// This is not automatically added to any output of this package,
	// it is provided as a convenience.
	Header = `<?xml version="1.0" encoding="UTF-8"?>` + "\n"
)

type Seller struct {
	Text     string `xml:",chardata"`
	Contacts struct {
		Text          string `xml:",chardata"`
		ContactMethod string `xml:"contact-method"`
	} `xml:"contacts"`
	Locations struct {
		Text     string `xml:",chardata"`
		Location struct {
			Text    string `xml:",chardata"`
			Address string `xml:"address"`
		} `xml:"location"`
	} `xml:"locations"`
}

type Offer struct {
	Text        string `xml:",chardata"`
	ID          string `xml:"id"`
	Seller      Seller `xml:"seller"`
	Title       string `xml:"title"`
	Description string `xml:"description"`
	Condition   string `xml:"condition"`
	Category    string `xml:"category"`
	ImagesDir   string `xml:"-"`
	Images      struct {
		Text  string   `xml:",chardata"`
		Image []string `xml:"image"`
	} `xml:"images"`
	Video string `xml:"video"`
	Price string `xml:"price"`
}

// StringShort, print offer and price
func (o *Offer) StringShort() string {
	return fmt.Sprintf(
		"%s %s",
		o.Title,
		ColorString(o.Price, "orange", "bold"),
	)
}

// If one digit number in num string, add some zeros in the line begin
func addZeros(num string) string {
	if len(num) == 1 {
		num = "0" + num
	}

	return num
}

func (o Offer) generateID() string {
	rand.Seed(time.Now().UTC().UnixNano())

	result := ""
	for i := 0; i < maxIDLen-8; i++ {
		result += strconv.Itoa(rand.Intn(10))
	}
	currentTime := time.Now()

	// last 2 minutes
	result += addZeros(strconv.Itoa(currentTime.YearDay() % 100))

	// last 2 hours
	result += addZeros(strconv.Itoa(currentTime.Hour()))

	// last 2 minutes
	result += addZeros(strconv.Itoa(currentTime.Second()))

	// last 2 nanoseconds
	result += addZeros(strconv.Itoa(currentTime.Nanosecond() % 100))

	return result
}

func (o Offer) ToGrabberItem() GrabberItem {
	price, err := strconv.Atoi(o.Price)

	if err != nil {
		log.Println(err)
	}

	return GrabberItem{
		Title:       o.Title,
		Price:       price,
		Category:    o.Category,
		Description: o.Description,
		Directory:   o.ImagesDir,
	}
}

// generateSimpleOffer add default parameters to all offers
func (o Offer) New() *Offer {
	newOffer := &Offer{
		ID: Offer{}.generateID(),
	}

	newOffer.Seller.Contacts.ContactMethod = "only-chat"
	newOffer.Seller.Locations.Location.Address = "Россия, Санкт-Петербург, метро Проспект Большевиков"
	newOffer.Condition = "used"

	return newOffer
}

type Feed struct {
	XMLName xml.Name `xml:"feed"`
	Text    string   `xml:",chardata"`
	Version string   `xml:"version,attr"`
	Offers  struct {
		Text   string  `xml:",chardata"`
		Offers []Offer `xml:"offer"`
	} `xml:"offers"`
}

func (f Feed) New() *Feed {
	return &Feed{
		Version: "1",
	}
}

func (f Feed) WriteToFile() {
	file, err := os.Create(baseDir + "products/products.xml")
	defer file.Close()

	output, err := xml.MarshalIndent(f, "", "    ")
	if err != nil {
		fmt.Printf("error: %v\n", err)
	}

	file.WriteString(Header)
	file.Write(output)
}

func (f Feed) ReadFile() Feed {

	content, err := ioutil.ReadFile(baseDir + "products/products.xml")
	if err != nil {
		log.Fatal(err)
	}

	if err := xml.Unmarshal(content, &f); err != nil {
		log.Fatal(err)
	}

	return f
}
