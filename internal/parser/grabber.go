package parser

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

const (
	avitoURL   = "https://www.avito.ru"
	profileURL = "https://www.avito.ru/user/1346034488ae90a5a9015100f265d97f/profile?id=2018569060&src=item&page_from=from_item_card&iid=2018569060"
)

type Grabber struct {
	Parser Parser
}

func (g Grabber) DeleteOffer() func(*cobra.Command, []string) {
	return func(cmd *cobra.Command, args []string) {
		// show item
		offers := g.Parser.ReadMDFile(baseDir + "products/items.md")

		if len(args) == 0 {
			for index, offer := range offers {
				fmt.Println(index+1, offer.Title, offer.Price)
			}
			return
		}

		deleting := make([]int, 0)
		// deleting numbers
		for _, arg := range args {
			selected, err := strconv.Atoi(arg)

			if err != nil {
				fmt.Println(arg, "is not a number. Skipping!")
				continue
			}
			selected--
			if selected < 0 {
				fmt.Println("Value must be positive.")
				continue
			}

			deleting = append(deleting, selected)
			fmt.Println("Appending", offers[selected].Title)
		}

		fmt.Println("-------------------------------")

		sort.Ints(deleting)
		fmt.Println("deleting", deleting)
		savedItems := make([]GrabberItem, 0)
		currentIndex := 0

		for i, offer := range offers {
			if deleting[currentIndex] != i {
				savedItem := offer.ToGrabberItem()
				savedItems = append(savedItems, savedItem)
			} else {
				if currentIndex+1 < len(deleting) {
					currentIndex++
				}
				fmt.Println("removing", baseDir+"site/static/"+offer.ImagesDir)
				err := os.RemoveAll(baseDir + "site/static/" + offer.ImagesDir)
				if err != nil {
					log.Println(err)
				}
			}
		}

		GrabberItem{}.Resave(savedItems)

		fmt.Println("Saved ", len(savedItems), "/", len(offers))
	}
}

func (g Grabber) SyncOffers() func(*cobra.Command, []string) {
	return func(cmd *cobra.Command, args []string) {
		// load all offers from xml file
		g.Parser = Parser{}.Init()

		elements := GetAllUserOffers(true)
		if len(elements) == 0 {
			fmt.Println(
				ColorString("Avito offers not founds. Try again later...", "red", ""),
			)
			return
		}

		offers := g.Parser.ReadMDFile(baseDir + "products/items.md")

		syncItems := make([]GrabberItem, 0)

		syncedCount := 0
		for _, savedItem := range offers {
			itemSynced := false

			// find saved item and update it
			for _, item := range elements {
				if item.Title == savedItem.Title {
					syncedCount++
					g.GrabItem(item.URL, &item)

					savedItem.Description = item.Description
					savedItem.Price = strconv.Itoa(item.Price)

					syncItems = append(syncItems, savedItem.ToGrabberItem())
					fmt.Println("syncing", item.Title, item.Price)
					itemSynced = true
				}
			}

			if !itemSynced {
				syncItems = append(syncItems, savedItem.ToGrabberItem())
			}
		}

		GrabberItem{}.Resave(syncItems)

		fmt.Println(
			ColorString("DONE: synced ", "green", "bold"), syncedCount, "/", len(offers))
	}
}

func (g Grabber) GrabProfile() func(*cobra.Command, []string) {
	return func(cmd *cobra.Command, args []string) {
		// Refactor withoutCache variable
		// load all offers from xml file
		g.Parser = Parser{}.Init()
		grabNumbers := make([]int, 0)
		offers := g.Parser.ReadMDFile(baseDir + "products/items.md")

		for _, arg := range args {
			if element, err := strconv.Atoi(arg); err == nil {
				grabNumbers = append(grabNumbers, element)
			}
		}

		elements := GetAllUserOffers(withoutCache)

		if len(grabNumbers) == 0 {
			fmt.Println(
				ColorString("Finded :", "blue", "bold"), len(elements), "items")
			for number, item := range elements {
				exists := false
				for _, offer := range offers {
					if offer.Title == item.Title {
						exists = true
						break
					}
				}
				itemString := ""
				if exists {
					itemString = ColorString("[S]", "red", "bold")
				} else {
					itemString = ColorString(strconv.Itoa(number), "green", "bold")
				}
				fmt.Println(
					itemString,
					item.Title,
					ColorString(strconv.Itoa(item.Price), "orange", "bold"),
				)
			}
			if withoutCache == false {
				fmt.Println(ColorString("Grabbered with cache...", "blue", "bold"))
			} else {
				fmt.Println(ColorString("Grabbered without cache...", "green", "bold"))
			}
			return
		}
		for _, current := range grabNumbers {
			selected := elements[current]
			fmt.Println("Saving", selected.Title, selected.Price)

			if g.Parser.OffersNames[selected.Title] != "" {
				fmt.Println("Already in database...")
				return
			}

			fmt.Println("Parsing link", selected.URL)
			g.GrabItem(selected.URL, &selected)
			fmt.Println(selected)
			fmt.Println("Saving item to products.md file")

			reader := bufio.NewReader(os.Stdin)
			fmt.Print("Enter directory -->: ")
			directory, err := reader.ReadString('\n')
			if err != nil {
				log.Println(err)
			}

			directory = strings.Replace(directory, "\n", "", 1)
			selected.Directory = directory

			if !strings.HasSuffix(directory, "/") {
				directory += "/"
			}

			for len(selected.Category) == 0 {
				fmt.Println("Enter item category ->")
				selected.Category = CategorySelector()
			}

			// Erase \n character
			os.MkdirAll(baseDir+"site/static/"+directory, os.ModePerm)
			OpenImagesFolder(directory)

			fmt.Println(selected)

			selected.SaveToFile()
		}
	}
}
