package parser

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

const (
	defaultFilename = "items.md"
	baseURL         = "https://likipiki.gitlab.io/md2xml/"

	// The base directory location
	baseDir = "./"
)

type Parser struct {
	OffersNames map[string]string
}

func (p Parser) Init() Parser {
	offersNames := make(map[string]string)
	allOffers := Feed{}.ReadFile()

	for _, offer := range allOffers.Offers.Offers {
		offersNames[offer.Title] = offer.ID
	}

	return Parser{
		OffersNames: offersNames,
	}
}

func ParseMd(filename string) func(*cobra.Command, []string) {
	return func(cmd *cobra.Command, args []string) {
		parser := Parser{}.Init()

		feed := Feed{}.New()
		offers := parser.ReadMDFile(baseDir + "products/items.md")
		feed.Offers.Offers = offers
		feed.WriteToFile()
	}
}

func (p Parser) ReadMDFile(filename string) []Offer {
	offers := make([]Offer, 0)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	newOffer := Offer{}.New()
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) > 0 {
			if line[0] == '#' {
				offers = p.appendNewOffer(offers, *newOffer)

				newOffer = Offer{}.New()
				newOffer.Title = line[2:]

				scanner.Scan()
				newOffer.Price = scanner.Text()

				scanner.Scan()
				newOffer.ImagesDir = scanner.Text()

				scanner.Scan()
				newOffer.Category = scanner.Text()
			} else {
				newOffer.Description += line + "\n"
			}
		} else {
			newOffer.Description += "\n"
		}
	}

	offers = p.appendNewOffer(offers, *newOffer)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return offers
}

func (p Parser) appendNewOffer(offers []Offer, newOffer Offer) []Offer {
	if newOffer.Title == "" {
		return offers
	}

	if p.OffersNames[newOffer.Title] != "" {
		fmt.Println("Find offer:", newOffer.ID, newOffer.Title)
		newOffer.ID = p.OffersNames[newOffer.Title]
	}

	files, err := ioutil.ReadDir(baseDir + "site/static/" + newOffer.ImagesDir)
	if err != nil {
		log.Fatal(err)
	}

	newOffer.Images.Image = filterImages(files, baseURL+newOffer.ImagesDir+"/")

	// FIXED \n on the end of string
	if strings.HasSuffix(newOffer.Description, "\n") {
		descLen := len(newOffer.Description)
		newOffer.Description = newOffer.Description[:descLen-len("\n")]
	}

	offers = append(offers, newOffer)

	return offers
}

func filterImages(files []os.FileInfo, url string) []string {
	filetypes := []string{
		".jpeg",
		".jpg",
		".png",
	}

	images := make([]string, 0)
	for _, f := range files {
		needAdd := false

		for _, filetype := range filetypes {
			if strings.HasSuffix(f.Name(), filetype) {
				needAdd = true
			}
		}

		if needAdd {
			images = append(
				images,
				url+f.Name(),
			)
		}
	}

	return images
}
