# MD2XML

This is my simple avito parser and O.Yandex.ru autoloading programm.

## Installing

```bash
go install gitlab.com/likipiki/md2xml ./cmd/md2xml
```

## Builiding
```bash
go build ./cmd/md2xml # or u can use
make
```

## Requirements

- [FZF](https://github.com/junegunn/fzf)
    - Installation:
    ```bash
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install
    ```

## TODO 

Main features
- [x] Change offer id generator logic, generate random number and add [year, date, time] numbers to the end of code
- [ ] Refactoring
  - [ ] Replace GrabberItem to Offer struct
  - [ ] Refactor without cache variable
- [x] Delete offers (remove offer info from items.md and delete images folder)
- [x] Grab several items (mdxml grab 1-4, to grab 1, 2, 3, 4 items)
- [x] List offers
- [x] Color items output

Grabbing:
- [ ] Open images folder when grabbing
- [ ] Grab multiple items
- [x] Cache grabbing requests
- [ ] if not cache file, grab without cache
