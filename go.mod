module gitlab.com/likipiki/md2xml

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/antchfx/htmlquery v1.2.3 // indirect
	github.com/antchfx/xmlquery v1.3.3 // indirect
	github.com/chromedp/cdproto v0.0.0-20210126020047-7ec7357d1463 // indirect
	github.com/geziyor/geziyor v0.0.0-20210128175025-129402d754a6 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/mattn/go-runewidth v0.0.10 // indirect
	github.com/mum4k/termdash v0.14.0 // indirect
	github.com/nsf/termbox-go v0.0.0-20210114135735-d04385b850e8
	github.com/prometheus/client_golang v1.9.0 // indirect
	github.com/prometheus/procfs v0.3.0 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/spf13/cobra v1.1.1
	github.com/yuin/goldmark v1.3.2 // indirect
	golang.org/x/mod v0.4.1 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210228012217-479acdf4ea46 // indirect
	golang.org/x/text v0.3.5 // indirect
	golang.org/x/tools v0.1.0 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
