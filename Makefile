default:
	go build ./cmd/md2xml/

install:
	go install ./cmd/md2xml

croscompile:
	gox -output "build/{{.Dir}}_{{.OS}}_{{.Arch}}" ./cmd/md2xml

clean:
	rm -rf doc-gen
